#' The application server-side
#' 
#' @param input,output,session Internal parameters for {shiny}. 
#'     DO NOT REMOVE.
#' @import shiny
#' @noRd
app_server <- function( input, output, session ) {
  # Your application server logic 
  vals <- assign.reactives(input, output, session)
  
  view_start(input, output, session)
  view_select(vals, input, output, session)
  view_copy(vals, input, output, session)
  view_fix(vals, input, output, session)
  view_rename(vals, input, output, session)
  view_evaluate(vals, input, output, session)
  
  copySD(vals, input, output, session)
  evaluateR(vals, input, output, session)
  renameR(vals, input, output, session)
  fixID(vals, input, output, session)
}
